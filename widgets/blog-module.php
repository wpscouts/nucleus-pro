<?php
namespace Nucleus\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Blog Module
 *
 * Elementor widget for displaying a blog post modules.
 *
 * @since 1.0.0
 */
class Blog_Module extends Widget_Base {

	public function get_name() {
		return 'blog-module';
	}

	public function get_title() {
		return __( 'Blog', 'elementor' );
	}

	public function get_icon() {
		return 'eicon-posts-grid';
	}


	/**
	 * A list of scripts that the widgets is depended in
	 * @since 1.3.0
	 **/
	public function get_script_depends() {
		return [ 'blog-module' ];
	}


	public function get_terms() {
		
		$blog_terms = get_terms('category');
		
		$blog_terms_options = array();
	
		foreach ($blog_terms as $term) {
			$blog_terms_options[$term->slug] = $term->name;
		}

		return $blog_terms_options;
	}


	protected function _register_controls() {

		// PORTFOLIO GRID - TAB
		$this->start_controls_section(
			'section_blog_grid',
			[
				'label' => __( 'Blog Grid', 'elementor' ),
			]
		);

		$this->add_control(
			'heading',
			[
				'label' => __( 'Title', 'nucleus' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
				'description' => 'If defined it will be displayed above posts grid.',
			]
		);

		$this->add_control(
			'category',
			[
				'label' => __( 'Category', 'elementor' ),
				'type' => Controls_Manager::SELECT2,
				'default' => null,
				'options' => $this->get_terms(),
				'multiple' => true,
				'label_block' => true,
				'description' => 'If no category is selected it will populate posts from all categories.',
			]
		);


		$this->add_control(
			'post-format',
			[
				'label' => __( 'Post Format', 'elementor' ),
				'type' => Controls_Manager::SELECT2,
				'default' => 'all-formats',
				'options' => [
					'all-formats' 			=> __( 'All Formats', 'elementor' ),
					'post-format-standard' 	=> __( 'Standard', 'elementor' ),
					'post-format-video' 	=> __( 'Video', 'elementor' ),
					'post-format-audio' 	=> __( 'Audio', 'elementor' ),
				],
				'label_block' => true,
			]
		);

		$this->add_control(
			'style',
			[
				'label' => __( 'Style', 'elementor' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'carousel-featured' => __( 'Carousel Featured', 'elementor' ),
					'carousel-regular' => __( 'Carousel Regular', 'elementor' ),
					'posts-grid' => __( 'Posts Grid', 'elementor' ),
					'posts-full' => __( 'Posts Full', 'elementor' ),
					'posts-full-grid' => __( 'Posts Full + Posts Grid', 'elementor' ),
				],
				'default' => 'style-1',
				'label_block' => true,
			]
		);

		$this->add_control(
			'count',
			[
				'label' => __( 'Count', 'elementor' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 5,
				],
				'range' => [
					'px' => [
						'min' => -1,
						'max' => 50,
					],
				],
			]
		);

		$this->add_control(
			'view',
			[
				'label' => __( 'View', 'elementor' ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$this->end_controls_section();

	}

	// RENDER ON FRONT-END ONLY
	protected function render() {

		// Widget Variable(s)
		$blog_heading 		= $this->get_settings( 'heading' );
		$blog_category 		= $this->get_settings( 'category' );
		$blog_post_format 	= $this->get_settings( 'post-format' );
		$blog_style 		= $this->get_settings( 'style' );
		$blog_count 		= $this->get_settings( 'count' );


		// WP_QUERY Arguments
		$blog_args = array(
			'post_type' 		=> 'post',
			'posts_per_page'    => $blog_count['size'],
			'tax_query'    		=> array(),
		);

		// Check if category(ies) is/are defined, if yes, inject it in 'tax_query' array.
		if ($blog_category) {

			$category_query = array(
				'taxonomy' => 'category',
				'field' => 'slug',
				'terms' => $blog_category
			);

			array_push($blog_args['tax_query'], $category_query);

		}

		// Check if post format is defined, if yes, inject it in 'tax_query' array.
		if ( $blog_post_format == 'post-format-standard' ) {

			$formart_query = array(
				'taxonomy' => 'post_format',
				'field' => 'slug',
				'terms' => array('post-format-aside', 'post-format-gallery', 'post-format-link', 'post-format-image', 'post-format-quote', 'post-format-status', 'post-format-audio', 'post-format-chat', 'post-format-video'),
				'operator' => 'NOT IN'
			);

			array_push($blog_args['tax_query'], $formart_query);

    	} else if ( $blog_post_format == 'post-format-audio' || $blog_post_format == 'post-format-video') {

			$formart_query = array(
				'taxonomy' => 'post_format',
				'field' => 'slug',
				'terms' => $blog_post_format,
			);

			array_push($blog_args['tax_query'], $formart_query);

    	}
 
		$blog_query = new \WP_Query($blog_args);

	?>

	<!-- BEGIN: PRIMARY CAROUSEL -->
	<section class="blog-module" data-layout="<?php echo $blog_style; ?>">

		<?php if ( $blog_query->have_posts() ) : ?>

			<?php if ($blog_heading) { ?>
				<h4 class="title"><?php echo $blog_heading; ?></h4>	
			<?php } ?>

			<main class="posts-loop">

				<?php $counter = 1; ?>

				<?php while ( $blog_query->have_posts() ) : $blog_query->the_post(); ?>
					<?php require __DIR__ . '/partials/blog/primary-loop.php'; ?>
					<?php $counter++; ?>
				<?php endwhile; ?>

				<?php wp_reset_postdata(); ?>

			</main>

			<footer class="carousel-nav">
				<a href="#" class="active"><span>01</span></a>
				<a href="#"><span>02</span></a>
				<a href="#"><span>03</span></a>
			</footer>

		<?php else: ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

	</section>
	<!-- END: PRIMARY CAROUSEL -->


		<?php
	}

}