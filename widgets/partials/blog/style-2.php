<?php
	// Helper Variable(s)
	$categories = get_the_category();
	$thumbnail_size = ($counter == 1) ? 'nucleus-blog-grid-4x' : 'nucleus-blog-grid-1x';

?>

<!-- PORTFOLIO ENTRY -->
<article id="post-<?php the_ID(); ?>" <?php post_class('grid-item'); ?>>

	<figure class="entry-thumbnail">
		
		<a href="<?php echo esc_url( get_permalink() ); ?>">
			<img src="<?php the_post_thumbnail_url( $thumbnail_size ); ?>">
		</a>

		<div class="entry-icons">
			<span class="play"><i class="fa fa-play" aria-hidden="true"></i></span>
		</div>

	</figure>

	<div class="entry-content">

		<div class="entry-meta header-meta">
			<span class="category"><?php echo esc_html( $categories[0]->name ); ?></span>
			<span class="duration">12:05</span>
		</div>

		<h3 class="entry-title">
			<a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a>
		</h3>

	</div>	

</article>