<?php
	// Helper Variable(s)
	$categories = get_the_category();
	$thumbnail_size = ($counter == 1) ? 'nucleus-blog-grid-2x' : 'nucleus-blog-grid-1x';

?>

<!-- PORTFOLIO ENTRY -->
<article id="post-<?php the_ID(); ?>" <?php post_class('grid-item'); ?>>

	<figure class="entry-thumbnail">
		<a href="<?php echo esc_url( get_permalink() ); ?>">
			<img src="<?php the_post_thumbnail_url( $thumbnail_size ); ?>">
		</a>
	</figure>

	<div class="entry-content">

		<div class="entry-meta header-meta">
			<span class="category"><?php echo esc_html( $categories[0]->name ); ?></span>
		</div>

		<h3 class="entry-title">
			<a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a>
		</h3>

		<div class="entry-excerpt"><p>A hard look at whether the rise comes from more awareness, better diagnosis—or something else </p></div>

		<div class="entry-meta footer-meta">
			<span class="timestamp">
				<a href="<?php echo esc_url( get_permalink() ); ?>">
					<?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ' . esc_html__( 'ago', 'nucleus' ); ?>	
				</a>
			</span>
			<span class="author"><?php the_author(); ?></span>
		</div>

	</div>	

</article>