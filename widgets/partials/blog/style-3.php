<?php
	// Helper Variable(s)
	$categories = get_the_category();

?>

<!-- PORTFOLIO ENTRY -->
<article id="post-<?php the_ID(); ?>" <?php post_class('grid-item'); ?>>

	<div class="entry-icons">
		<span class="play"><i class="fa fa-play" aria-hidden="true"></i></span>
	</div>

	<div class="entry-content">

		<h3 class="entry-title">
			<a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a>
		</h3>

		<div class="entry-meta footer-meta">
			<span class="timestamp">
				<a href="<?php echo esc_url( get_permalink() ); ?>">
					<?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ' . esc_html__( 'ago', 'nucleus' ); ?>	
				</a>
			</span>
			<span class="author"><?php the_author(); ?></span>
			<span class="duration">12:05</span>
		</div>

	</div>	

</article>