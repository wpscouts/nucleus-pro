<?php
	// Helper Variable(s)
	$categories = get_the_category();

	switch ($blog_style) {
		case 'carousel-featured':
	    $thumbnail_size = 'nucleus-blog-carousel-featured';
	    break;		
		case 'carousel-regular':
			// $thumbnail_size = ($counter % 2 != 0) ? 'nucleus-blog-carousel-regular-odd' : 'nucleus-blog-carousel-regular-even';
			$thumbnail_size = 'nucleus-blog-carousel-regular-odd';
	    break;		
		case 'posts-full':
	    $thumbnail_size = 'nucleus-blog-full';
	    break;
		case 'posts-grid':
	    $thumbnail_size = 'nucleus-blog-grid';
	    break;
		case 'posts-full-grid':
			$thumbnail_size = ($counter == 1) ? 'nucleus-blog-full' : 'nucleus-blog-grid';
	    break;
		default:
	    $thumbnail_size = 'nucleus-blog-full';
	}

?>

<!-- PORTFOLIO ENTRY -->
<article id="post-<?php the_ID(); ?>" <?php post_class('grid-item'); ?>>

	<figure class="entry-thumbnail">
		<a href="<?php echo esc_url( get_permalink() ); ?>">
			<img src="<?php the_post_thumbnail_url( $thumbnail_size ); ?>">
		</a>
	</figure>

	<div class="entry-content">

		<div class="entry-meta">
			<span class="category"><?php echo esc_html( $categories[0]->name ); ?></span>
			<span class="date">March 21, 2017</span>
		</div>

		<h3 class="entry-title">
			<a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a>
		</h3>

		<div class="entry-excerpt"><?php the_excerpt(); ?></div>

		<a class="read-more" href="#">Read More</a>

	</div>	

</article>