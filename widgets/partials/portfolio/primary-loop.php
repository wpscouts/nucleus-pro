<?php
  // Helper Variable(s)
  $folio_terms          = implode(', ', nucleus_get_term_fields('portfolio_category', 'name'));
  $folio_permalink      = get_post_meta(get_the_ID(), 'custom_url', true) != false ? esc_url( get_post_meta(get_the_ID(), 'custom_url', true) ) : esc_url( get_permalink() );
  $thumbnail_dimension  = get_field('thumbnail_dimension');
  $thumbnail_classes    = $thumbnail_dimension['height']. ' ' .$thumbnail_dimension['width'];
?>

<!-- PORTFOLIO ENTRY -->
<article id="post-<?php the_ID(); ?>" <?php post_class("grid-item $thumbnail_classes"); ?>>

  <a class="entry-link" href="<?php echo $folio_permalink; ?>" title="<?php the_title(); ?>" >
    
    <figure class="entry-thumbnail">
      <img src="<?php the_post_thumbnail_url(); ?>">
    </figure>

    <header class="entry-caption">
      <div class="inner-wrap">
        <h3 class="entry-title"><?php the_title(); ?></h3>
        <?php if ( $folio_terms ) { ?>
          <span class="entry-meta"><?php echo esc_html( $folio_terms ); ?></span>
        <?php } ?>
      </div>
    </header>

  </a>    

</article>