( function( $ ) {
	
  var WidgetBlogModuleHandler = function( $scope, $ ) {
    console.log( $scope );

    $('.blog-module[data-layout="carousel-featured"] .posts-loop').flickity({
      pageDots: false,
      initialIndex: 2,
      wrapAround: true
    });

    $('.blog-module[data-layout="carousel-regular"] .posts-loop').flickity({
      cellAlign: 'left',
      pageDots: false,
      initialIndex: 2,
      wrapAround: true
    });

    $('.flickity-prev-next-button').html('<i class="fi fi-thin-arrow" aria-hidden="true"></i>');
  };
  
  // Make sure you run this code under Elementor..
  $( window ).on( 'elementor/frontend/init', function() {
    elementorFrontend.hooks.addAction( 'frontend/element_ready/blog-module.default', WidgetBlogModuleHandler );
  } );  

} )( jQuery );



// $('.primary-carousel .carousel-nav').on( 'click', 'a', function(e) {
//   e.preventDefault();
//   var index = $(this).index();
//   $('.primary-carousel .carousel-nav a').removeClass('active');
//   $(this).addClass('active');
//   $carousel.flickity( 'select', index );
// });