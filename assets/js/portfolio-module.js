( function( $ ) {
	
  var WidgetPortfolioModuleHandler = function( $scope, $ ) {
    console.log( $scope );
    $('body.elementor-editor-active .main-carousel').flickity();
  };
  
  // Make sure you run this code under Elementor..
  $( window ).on( 'elementor/frontend/init', function() {
    elementorFrontend.hooks.addAction( 'frontend/element_ready/portfolio-module.default', WidgetPortfolioModuleHandler );
  } );  

} )( jQuery );
